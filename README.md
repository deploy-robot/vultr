# vultr-api-rust

A pure Rust Vultr API binding.

## Examples

### Example blocking

It needs to have the feature "blocking" enabled.

```toml
vultr = { version = "*", features = ["blocking"] }
```

```rust
use vultr::VultrApi;
use vultr::VultrError;

fn main() -> Result<(), VultrError> {
    let api = VultrApi::new("<KEY>");
    let account = api.get_account_info()?;
    println!("ACCOUNT: {:?}", account);

    let regions = api.get_regions()?;
    println!("REGIONS: {:?}", regions);
        
    let plans = api.get_plans()?;
    println!("PLANS: {:?}", plans);
    
    let os = api.get_os_list()?;
    println!("OS: {:?}", os);
    Ok(())
}
```

### Example async

```toml
vultr = { version = "*" }
```

```rust
use vultr::VultrApi;
use vultr::VultrError;

#[async_std::main]
async fn main() -> Result<(), VultrError> {
    let api = VultrApi::new("<KEY>");
    let account = api.get_account_info_async().await?;
    println!("ACCOUNT: {:?}", account);
    Ok(())
}
```

## Features

* "default" - use nativetls
* "default-rustls" - use rusttls
* "blocking" - enable blocking api
* "rustls" - enable rustls for reqwest
* "nativetls" - add support for nativetls DEFAULT
* "gzip" - enable gzip in reqwest
* "brotli" - enable brotli in reqwest
* "deflate" - enable deflate in reqwest

## TODO

* [ ] Documentation
* [ ] Full api support
