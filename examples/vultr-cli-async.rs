use vultr::VultrApi;
use vultr::VultrError;
use vultr::VultrInstanceType;
use vultr::VultrOS;

use std::env;

#[async_std::main]
async fn main() -> Result<(), VultrError> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        println!("Call program with the following:");
        println!("{} VULTR_API_KEY DOMAIN", args[0]);
        std::process::exit(1);
    }

    let result = do_stuff(&args[1], &args[2]).await;
    match result {
        Ok(_) => {
            println!("Finished sucessfully");
        }
        Err(e) => {
            println!("Error: {}", e);
        }
    }
    Ok(())
}

async fn do_stuff(api_key: &str, domain: &str) -> Result<(), VultrError> {
    let api = VultrApi::new(api_key);

    let account = api.get_account_info_async().await?;
    println!("ACCOUNT: {:?}", account);

    let new_domain = api.create_dns_domain_async(domain, None, false).await?;
    println!("CREATED DOMAIN: {:?}", new_domain);

    let old_domain = api.get_dns_domain_async(domain).await?;
    println!("GET DOMAIN: {:?}", old_domain);

    let record = api
        .create_dns_domain_record_async(domain, "A", "www", "10.0.0.8", None, None)
        .await?;
    println!("RECORD CREATED: {:?}", record);

    let records = api.get_dns_domain_records_async(domain).await?;
    println!("RECORDS: {:?}", records);

    let record = api.delete_dns_domain_record_async(domain, &record.id).await;
    println!("RECORD DELETED: {:?}", record);

    let domains = api.get_dns_domain_list_async().await?;
    println!("DOMAIN LIST: {:?}", domains);

    let old_domain = api.delete_dns_domain_async(domain).await?;
    println!("DEL DOMAIN: {:?}", old_domain);

    let regions = api.get_regions_async().await?;
    println!("REGIONS: {:?}", regions);

    let plans = api.get_plans_async().await?;
    println!("PLANS: {:?}", plans);

    let mut os = api.get_os_list_async().await?;
    println!("OS: {:?}", os);

    let ssh_key = api.create_sshkey_async("test", "xxx").await?;
    println!("SSH KEY CREATED: {:?}", ssh_key);

    let ssh_key = api.get_sshkey_async(ssh_key.id).await?;
    println!("SSH KEY: {:?}", ssh_key);

    let ssh_keys = api.get_sshkey_list_async().await?;
    println!("SSH KEYS: {:?}", ssh_keys);

    let instances = api.get_instance_list_async().await?;
    println!("INSTANCE LIST: {:?}", instances);

    let ubuntu_list: Vec<VultrOS> = os
        .drain(..)
        .filter(|item| item.family.contains("ubuntu"))
        .collect();

    let instance = api
        .create_instance(
            &regions[0].id,
            &plans[0].id,
            VultrInstanceType::OS(ubuntu_list[0].id),
        )
        .enable_ipv6(true)
        .label("mylabel")
        .sshkey_id(&ssh_key.id)
        .backups(false)
        .ddos_protection(false)
        .activation_email(false)
        .hostname("myhostname")
        .tags(vec![String::from("mytag")])
        .run_async()
        .await?;
    println!("INSTANCE CREATE: {:?}", instance);

    let ssh_key = api.delete_sshkey_async(ssh_key.id).await?;
    println!("SSH KEY DELETED: {:?}", ssh_key);

    let instance = api.get_instance_async(instance.id).await?;
    println!("INSTANCE GET: {:?}", instance);

    let instance = api.delete_instance_async(instance.id).await?;
    println!("INSTANCE DELETE: {:?}", instance);

    Ok(())
}
